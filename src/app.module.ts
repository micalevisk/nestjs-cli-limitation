import { Module } from '@nestjs/common';

import { FooCommand } from './foo.command';
import { FooQuestions } from './foo.questions';

@Module({
  imports: [FooCommand, FooQuestions],
})
export class AppModule {}
