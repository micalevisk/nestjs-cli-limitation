import { QuestionSet, Question } from 'nest-commander'

@QuestionSet({ name: 'foo' })
export class FooQuestions {
  @Question({
    name: 'catName',
    message: 'What is the name of your cat?',
  })
  parseCatName(str: string): string {
    return str
  }
}
