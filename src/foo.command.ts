import { Command, CommandRunner, InquirerService } from 'nest-commander'

@Command({
  name: 'foo',
  options: { isDefault: true },
})
export class FooCommand implements CommandRunner {
  constructor(private readonly promptService: InquirerService) {}

  async run(inputs: string[], cliSuppliedOpts: Record<string, any> = {}) {
    const optsFoo = await this.promptService.ask('foo', cliSuppliedOpts)

    console.log({ inputs, cliSuppliedOpts, optsFoo })
  }
}
