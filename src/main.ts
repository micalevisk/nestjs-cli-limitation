import { CommandFactory } from 'nest-commander'

import { AppModule } from './app.module'

function bootstrap() {
  return CommandFactory.run(AppModule)
}

bootstrap()
